<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TraineeshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traineeship', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->integer('category_id')->unsigned()->nullable()->default(null);
            $table->string('title');
            $table->string('location');
            // $table->enum('status', ['open', 'closed'])->default('open');
            $table->text('description');
            $table->timestamp('lastDay');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('company');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('set null');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traineeship');
    }
}