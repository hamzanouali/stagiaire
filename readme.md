1- Download Laragon server from:  https://laragon.org/download/#Edition

NOTE: download full or lite version.

2- Open Laragon > Open Terminal

3- Past this `git clone https://hamzanouali@bitbucket.org/hamzanouali/stagiaire.git` to the Terminal

4- Enter again `cd stagiaire` to the Terminal.

5- Enter: `composer install`

6- Rename `.env.example` file to `.env`

7- In Laragon > Open Database > Create a new database `stagiaire`

8- Now, Go to www/stagiaire folder > Open .env file with notpad++ or any editor.

9- Edit these values like that:

`DB_DATABASE=stagiaire`

`DB_USERNAME=root`

`DB_PASSWORD=`

Save the file and close it.

10- Enter this again:
`php artisan migrate && php artisan voyager:admin admin@stagiaire.com --create && php artisan db:seed --class=VoyagerDatabaseSeeder && php artisan hook:setup && php artisan storage:link && composer dump-autoload`

11- Enter: `php artisan key:generate`

12- Enter: `php artisan server`

Open http://127.0.0.1:8000 in your browser.

For admin:
Open http://127.0.0.1:8000/admin

For any questions, contact me at: `hamza.nouali.business@gmail.com`