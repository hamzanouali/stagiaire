<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profile', 'UserController@show')->middleware('auth');
Route::get('/profile/mon-cv', 'UserController@renderCV')
    ->middleware(['auth', 'accountType:person']);

Route::post('/profile/mon-cv', 'UserController@uploadCV')
    ->middleware(['auth', 'accountType:person'])->name('upload_cv');
Route::get('/profile/notifications', 'UserController@renderNotifications')
    ->middleware(['auth']);
Route::get('/profile/notifications/mark-as-read', 'UserController@markAllNotificationAsRead')
    ->middleware(['auth']);

Route::post('/profile/update', 'UserController@updateInfo')
    ->middleware('auth')->name('update_profile');
    
Route::post('/profile/avatar/update', 'UserController@saveUserAvatar')
    ->middleware('auth');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 ** Traineeship Routes 
 */
Route::get('traineeships', 'TraineeshipController@index');

Route::get(
    'traineeships/filter'/*?location={location}&category={category_id}*/, 
    'TraineeshipController@filter')
    ->where([
        'category_id' => '[0-9]+',
        'location' => '[a-zA-Z0-9]+'
    ]);    

Route::get('/traineeships/show/{id}', 'TraineeshipController@showOne1')
    ->where('id','[0-9]+'); 

Route::get('profile/traineeship/create', function() {
    return view('traineeship.create', ['action' => 'create']);
})->middleware(['auth','accountType:company', 'CompleteUserProfile'])->name('create_traineeship');

Route::get('profile/traineeship/me', 'TraineeshipController@showMy')
    ->middleware(['auth','accountType:company']);

/**
 * show one traineeship
 * */    
//search for questions
Route::get('/traineeships/search/{title}','TraineeshipController@search')
    ->where(['title' => '[a-z\p{Arabic}\sA-Z0-9]+','skip' => '[0-9]+','take' => '[0-9]+']);


Route::get('/profile/traineeships/{id}', 'TraineeshipController@showOne')
    ->where('id','[0-9]+')
    ->middleware(['auth','accountType:company']);  

Route::post('profile/traineeship/create', 'TraineeshipController@store')
    ->middleware(['auth','accountType:company', 'CompleteUserProfile'])->name('store_traineeship');

Route::post('/traineeship/update/{id}', 'TraineeshipController@update')
    ->middleware(['auth','accountType:company'])->name('update_traineeship');  

Route::get('/traineeship/update/{id}', 'TraineeshipController@edit')
    ->where('id','[0-9]+')
    ->middleware(['auth','accountType:company']);

Route::delete('/traineeship/delete/{id}', 'TraineeshipController@destroy')
    ->where('id','[0-9]+')
    ->middleware(['auth','accountType:company']);

/**
 ** Subscribe Routes 
 */    
Route::post('subscribe/create', 'SubscribeController@store')
    ->middleware(['auth','accountType:person', 'CompleteUserProfile']); 
Route::post('subscribe/approve-subscription', 'SubscribeController@approveSubscription')
    ->middleware(['auth','accountType:company'])->name('approve_subscription');     
Route::post('subscribe/delete/{id}', 'SubscribeController@destroy')
    ->where('id','[0-9]+')
    ->middleware(['auth','accountType:person']);

