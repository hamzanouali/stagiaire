<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traineeship extends Model
{
    public $table = "traineeship";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'title', 'lastDay', 'company_id', 'location', 'description',
    ];

    protected function company() {
        return $this->belongsTo('App\Company');
    }

    protected function subscription() {
        return $this->hasOne('App\Subscribe');
    }

    protected function category() {
        return $this->belongsTo('App\Category');
    }

}
