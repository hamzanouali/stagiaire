<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';

    protected $fillable = ['user_id', 'companyName'];

    protected function traineeships() {
        return $this->hasMany('App\Traineeship');
    }

    protected function user() {
        return $this->belongsTo('App\User');
    }
}
