<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'person_id', 'traineeship_id'
    ];

    protected function traineeship() {
        return $this->belongsTo('App\Traineeship');
    }

    protected function person() {
        return $this->belongsTo('App\Person');
    }
}
