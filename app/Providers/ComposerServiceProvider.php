<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            ['traineeship/*', 'auth.profile', 'auth.cv', 'layouts.sideNavTopNav'], 
            'App\Http\View\Composers\ProfileComposer'
        );
        
        View::composer(
            ['traineeship/*', 'auth.profile', 'auth.cv', 'layouts.sideNavTopNav' , 'traineeship.index', 'traineeship.showOne1'], 
            'App\Http\View\Composers\CategoriesComposer'
        );
    }
}