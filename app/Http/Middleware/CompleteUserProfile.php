<?php

namespace App\Http\Middleware;

use Closure;

class CompleteUserProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = \Auth::user();
        $accountType = $user->accountType;

        if($accountType === 'person') {
            // in case, user did not complete his profile
            if(!$user->person->category_id || strlen($user->person->university) == 0) {
                
                return redirect('/profile')->withErrors([
                    'error_message' => 'vous devez completer votre profile',
                    'category_id' => 'required',
                    'university' => 'required',
                ]);                

            // in case, user don't have a cv yet    
            } else if(!file_exists( public_path().'/storage/cv/users/'.$user->id.'.pdf')) {
                return redirect('/profile/mon-cv')->withErrors(['error_message' => 'vous devez telechargez votre cv']);
            }
        } else if($accountType === 'company') {
            // same for company profile
            if(strlen($user->company->companyName) === 0) {
                
                return redirect('/profile')->withErrors([
                    'error_message' => 'vous devez completer votre profile', 
                    'companyName' => 'required'
                ]);
            }
        }
        return $next($request);
    }
}
