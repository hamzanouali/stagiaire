<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;

class ProfileComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(!\Auth::check()) return false;

        if(\Auth::user()->accountType == "company") {
            /**
             * get company info
             */
            $user = \DB::table('users')
            ->join('company', 'users.id', '=', 'company.user_id')
            ->where('users.id', \Auth::user()->id)
            ->first();


        } else {
            /**
             * get person info
             */
            $user = \DB::table('users')
            ->join('person', 'users.id', '=', 'person.user_id')
            ->where('users.id', \Auth::user()->id)
            ->first();

        }

        /**
         * in this case the user is loged in , but his account does not exist
         */
        if(!$user) {
            \Auth::logout();
            return redirect('/login');
        }

        $view->with(['user' => $user]);
    }
}