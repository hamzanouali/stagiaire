<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;

class CategoriesComposer
{

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = \DB::table('categories')->get();
        $view->with(['categories' => $categories]);
    }
}