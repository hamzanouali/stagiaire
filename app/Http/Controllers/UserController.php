<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{


    public function saveUserAvatar(Request $request) {
        $validator = Validator::make($request->all(), [
            'avatar'=> 'required|string',
        ])->validate();
        
        $png_url = \Auth::user()->id.".png";
        $path = public_path().'/storage/users/' . $png_url;
        
        /**
         * getting and saving base64 avatar in png format
         */
        Image::make(file_get_contents($request->avatar))->save($path);     
        
        // update database
        \App\User::where('id', \Auth::user()->id)->update([
            'avatar' => 'users/'.\Auth::user()->id.'.png'
        ]);

        return 'success';
    }

    /**
     * shows the profile
     */
    public function show() {
        return view('auth.profile');
    }

    /**
     ** Updates user info
     */
    public function updateInfo(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'firstName'=> 'required|string|max:255',
            'lastName'=> 'required|string|max:255',
            'accountType' => 'required|string|in:company,person',
            'address' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.\Auth::user()->id.',id',
        ])->validate();

        /**
         ** get user_id 
         */
        $user_id = \Auth::user()->id;

         \App\User::whereId($user_id)->update([
            'firstName'=> $request->firstName,
            'lastName'=> $request->lastName,
            'email' => $request->email,
            'address' => $request->address,
        ]);   

        /**
         ** Update company account info  
         */
        if(\Auth::user()->accountType == 'company') {


            $validator = Validator::make($request->all(), [
                'companyName' => 'required|string'
            ])->validate();

            /**
             ** update company table 
             */
            \DB::table('company')->where('user_id', $user_id)->update([
                'companyName' => $request->get('companyName')
            ]);
            return response()->view('auth.profile', ['message' => 'updated successfully !'], 201);

        /**
         ** update person account info 
         */ 
        } else {
            
            $validator = Validator::make($request->all(), [
                'category_id' => 'required|numeric',
                'university' => 'required|string|max:255',
            ])->validate();

            /**
             ** update person table 
             */
            \DB::table('person')->where('user_id', $user_id)->update([
                'category_id' => $request->get('category_id'),
                'university' => $request->get('university')
            ]);
            
            session(['success' => 'updated successfully !']);
            return response()->view('auth.profile');
        }
    }

    public function renderCV() {
        return view('auth.cv');
    }

    public function uploadCV(Request $request) {

        $cv = $request->cv;
        // Handle File Upload
        if($cv) {
            // Get filename with extension            
            $filenameWithExt = $cv->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);            
           // Get just ext
            $extension = $cv->getClientOriginalExtension();
            //Filename to store
            $fileNameToStore = \Auth::user()->id.'.'.$extension;                       

            // before i need to delete the existing cv
            if(file_exists(public_path().'/storage/cv/users/'.\Auth::user()->id.'.pdf')) {
                unlink(public_path().'/storage/cv/users/'.\Auth::user()->id.'.pdf');
            }

            // Upload Image
            $path = $cv->storeAs('public/cv/users', $fileNameToStore);
        } else {
            return redirect()->back();
        }
        
        return redirect()->back();
    }

    public function renderNotifications() {
        return view('auth.notifications');
    }

    public function markAllNotificationAsRead() {
        \Auth::user()->unreadNotifications->markAsRead();
        return redirect('/profile/notifications');
    }
}
