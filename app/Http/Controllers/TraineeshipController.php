<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Traineeship;

class TraineeshipController extends Controller
{

    /**
     * holds the value that is intended to be in paginate method
     */
    protected $paginateAmmount = 2;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $traineeships = \App\Traineeship::paginate($this->paginateAmmount);
        $category = \DB::table('categories')->get();

        return view('traineeship.index', [
            'traineeships' => $traineeships, 
            'categories' => $category
        ]);
    }

    public function search($title){

        // empty string
        if(empty($title) || $title == ' ') return '';

        // convert title to lower case
        //$title = strtolower($title);

        // number of words
        $num_of_words = count(explode(" ",$title));

        // last position of white space
        $last_white_space = strrpos($title," ");
        
        if($last_white_space == 0 && !empty($title)) {
            $traineeships = \App\Traineeship::where('title','LIKE','%'.$title.'%')->paginate($this->paginateAmmount);
            return view('traineeship.index', ['traineeships' => $traineeships]);
        }else if($last_white_space == 0){
            return view('traineeship.index', ['traineeships' => []]);
        }

        // loop through title words
        while($last_white_space != 0) {
            $title = substr($title,0,$last_white_space);
            // search for questions by title
            // then push the result to the collection
            $res = \App\Traineeship::where('title','LIKE','%'.$title.'%')->paginate($this->paginateAmmount);
            if($res->count() > 0){
                return view('traineeship.index', ['traineeships' => $res]);
            }
            $last_white_space = strrpos($title," ");
        }
        
        return view('traineeship.index', ['traineeships' => $res]);
    }

    /**
     * filters result and render all traineeships
     * 
     */
    public function filter()
    {
        
        $category_id = app('request')->get('category'); 
        $location = app('request')->get('location');

        /**
         * category_id must be converted from '1,2,3' to [1,2,3]
         */
        /* $category_id = explode(',', $category_id);
        foreach ($category_id as $key => $value) {
            $category_id[$key] = intval($value);
        } */

        // category_id length = 1 and category_id[0] == 0
        // that means search for all
        /* if(count($category_id) && $category_id[0] == 0) {
            $category_id = 'all';
        }

        // case 1: search all using category & all using location
        if($category_id == 'all' && $location == 'all') {
            // redirect to index page
            return redirect('/traineeships');
        }

        // case 2: search all using only category
        if($category_id == 'all' && $location != 'all') {
            $traineeships = \App\Traineeship::where('location','like', '%'.$location.'%')
                ->paginate($this->paginateAmmount);
        } */

        // case 3: search all using only location
        if($category_id != 'all' && $location == 'all') {
            $traineeships = \App\Traineeship::where('category_id','in', '('.$category_id.')' )
            ->paginate($this->paginateAmmount);
        }

        // case 4: user is fitering using both category and location
        /* if($category_id[0] != 0 && strlen($location)) {
            $traineeships = \App\Traineeship::where('category_id', $category_id)
                ->where('location','like', '%'.$location.'%')
                ->paginate($this->paginateAmmount);
        }  */

        return view('traineeship.index', ['traineeships' => $traineeships]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            "category_id" => "required|numeric",
            "title" => "required|string",
            "description" => "required|string",
            "lastDay" => "required|string",
            "location" => "required|string"
        ])->validate();

        $company_id = \App\User::find(\Auth::user()->id)->company->id;
            
        $res = Traineeship::create([
            "company_id" => $company_id,
            "category_id" => $request->category_id,
            "title" => $request->title,
            "description" => $request->description,
            "lastDay" => $request->lastDay,
            "location" => $request->location
        ]);
        
        return redirect()->back()->with('success', __('Traineeship was created successfully.')); 
    
    }

    public function showMy() {
        $company_id = \App\User::find(\Auth::user()->id)->company->id;

        $traineeships = \App\Traineeship::where('company_id', $company_id)->paginate(15);

        return view('traineeship.show', ['traineeships' => $traineeships]);
    }

    public function showOne($id) {
        $traineeship = \App\Traineeship::where('id', $id)->first();

        return view('traineeship.showOne', ['traineeship' => $traineeship]);
    }

    public function showOne1($id) {
        $traineeship = \App\Traineeship::where('id', $id)->first();

        return view('traineeship.showOne1', ['traineeships' => [$traineeship] ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /**
         * case: current user does not own this data
         */
        if(\App\Traineeship::whereId($id)->first()->company->user_id !== \Auth::user()->id) {
            return abort(403);
        }

        $traineeship = \App\Traineeship::whereId($id)->first();

        return view('traineeship.create', [
            'action' => 'update', 
            'traineeship' => $traineeship
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            "category_id" => "required|numeric",
            "title" => "required|string",
            "description" => "required|string",
            "lastDay" => "required|string",
            "location" => "required|string"
        ])->validate();
        
        Traineeship::whereId($id)
            ->update([
                "category_id" => $request->category_id,
                "title" => $request->title,
                "description" => $request->description,
                "lastDay" => $request->lastDay,
                "location" => $request->location
            ]);

        return redirect()->back()->with('success', 'your data has been updated with success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Traineeship::whereId($id)
            ->delete();

        return response()->json('Traineeship was deleted successfully.', 201);
    }
}
