<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Subscribe;
use App\Notifications\Subscriptions;
use App\Notifications\Messages;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class SubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * get person id
         */
        $person = \DB::table('person')->where('user_id', \Auth::user()->id)
        ->first(['id']);
        
        /**
         * validate inputs
         */
        Validator::make($request->all(), [
            "traineeship_id" => "required|numeric"
        ])->validate();

        /**
         * insert subscription into database
         */
        $subscribtion = new \App\Subscribe([
            'person_id' => $person->id,
            'traineeship_id' => $request->get('traineeship_id')
        ]);
        $subscribtion->save();

        /**
         * get company in order to send it a notification
         */
        $traineeship = \App\Traineeship::whereId($request->traineeship_id)->first();
        $companyUser = $traineeship->company->user;

        $companyUser->notify(new Subscriptions([
            'user_id' => \Auth::user()->id,
            'title' => $traineeship->title,
            'subscription_id' => $subscribtion->id
        ]));
        
        /**
         * return success response
         */
        return redirect()->back();
    }

    public function approveSubscription(Request $request)
    {
        $this->validate($request, [
            'subscription_id' => 'required|numeric',
            'message' => 'required|string|max:255'
        ]);

        /**
         * get subscription
         */
        $subscribtion = \App\Subscribe::whereId($request->subscription_id)->first();


        $person = \App\Person::whereId($subscribtion->person_id)->first();
        $personUser = $person->user;

        /**
         * notify person that he has been approved
         */
        $traineeship = \App\Traineeship::whereId($subscribtion->traineeship_id)->first();

        /**
         * check wether this user owns this traineeship
         */
        if($traineeship->company->user_id !== \Auth::user()->id) {
            return abort(403);
        }

        $personUser->notify(new Messages([
            /**
             * user_id in this case belongs to company
             */
            'user_id' => \Auth::user()->id,
            'title' => $traineeship->title,
            'message' => $request->message
        ]));

        /**
         * move traineeship to archive table by removing it from 
         * it's original table (traineeships)
         */
        // move to archive
        $archivedTraineeship = \App\Archive::create([
            'company_id' => $traineeship->company_id,
            'category_id' => $traineeship->category_id,
            'title' => $traineeship->title,
            'location' => $traineeship->location,
            'description' => $traineeship->description,
            'lastDay' => $traineeship->lastDay
        ]);
        $archivedTraineeship->save();
        // delete subscriptions first
        \App\Subscribe::where('traineeship_id', $traineeship->id)->delete();
        // delete from traineeships
        \App\Traineeship::destroy($traineeship->id);

        /**
         * delete notification
         */
        \DB::table('notifications')->whereId($request->notification_id)->delete();

        /**
         * return success response
         */
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /**
         * get person id
         */
        $person = \DB::table('person')->where('user_id', \Auth::user()->id)
        ->first(['id']);
        
        /**
         * validate inputs
         */
        Validator::make($request->all(), [
            "traineeship_id" => "required|numeric"
        ])->validate();

        Subscribe::where('traineeship_id', $request->traineeship_id)
            ->where('person_id', $person->id)
            ->delete();

        return redirect()->back();
    }
}
