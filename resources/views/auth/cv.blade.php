@extends('layouts.sideNavTopNav')

@section('content_1')
<div style="padding: 15px;" class="w-full mx-auto mt-8">
        
    <!-- success alert used by javascript -->
    <div style="display:none;" id="js-alert-success" style="padding: 15px;" class="animated fadeInUp mt-5">
      <div style="padding: 15px;" class="mt-5">
        <p style="padding: 15px;" class="cls-alert"><i class="fas fa-check-circle"></i>
          <span class="ml-2"></span></p>
      </div>
    </div>

    @error('error_message')
    <!-- error alert used by javascript -->
    <div id="js-alert-error"  style="padding: 15px;" class="animated fadeInUp mt-5">
      <p style="padding: 15px;" class="cls-alert-error">
        <i class="fas fa-times-circle" style=""></i>
        <span class="ml-2">{{ $message }}</span></p>
    </div>
    @enderror

    @if(file_exists( public_path().'/storage/cv/users/'.\Auth::user()->id.'.pdf'))
      <div style="padding: 0px 15px;">
        <div class="bg-black">
          <object data="{{ asset('storage/cv/users/2.pdf') }}" type="application/pdf" width="100%" height="500px;">
            <p>Alternative text - include a link <a href="myfile.pdf">to the PDF!</a></p>
          </object>
        </div>
      </div>
    @endif

    <!-- change image alert -->
    <div style="padding: 15px;" class="mt-5">
      <p style="padding: 15px;" class="cls-alert-info">
        <i class="far fa-lightbulb" style=""></i>
        <span class="ml-2" style="">vous devez télécharger votre cv</span>
      </p>
    </div>

    <form onsubmit="loading.start()" class="w-full p-0 flex flex-wrap" method="post" action="{{ route('upload_cv') }}" enctype="multipart/form-data" >
      @csrf

      <div style="padding: 15px;" class="w-full font-thin text-lg">
        <!-- drag and drop container --> 
        <div id="draggContainer" draggable="true" class="w-full font-thin h-48 text-center p-12 border text-2xl rounded leading-normal text-grey-darker uppercase bg-grey-lightest relative">
          télécharger le fichier cv ici (seuls les fichiers pdf, doc ou docx sont acceptés)
          <input type="file" name="cv" onchange="fileUploaded()" class="absolute pin w-full h-full opacity-0">
        </div>
      </div>
      
      <div class="w-full hidden p-5 items-start mt-5">
        <button id="submitForm" class="px-8 py-3 cls-btn-primary cursor-pointer">enregistrer</button>
      </div>
    </form>

  </div>

<script>
  function fileUploaded() {

    const fileType = document.querySelector('input[type=file]').files[0].type;
    const validation = ['application/pdf', 'application/doc', 'application/docs'].includes(fileType);

    if(!validation) {
      document.querySelector('input[type=file]').value = '';      
      return swal('Erreur', 'seuls les pdf, doc et docx sont supportés', 'error');
    }

    swal("voulez-vous continuer à télécharger votre cv?", {
      buttons: {
        annuler: true,
        oui: true,
      },
    })
    .then((value) => {
      switch (value) {
    
        case "oui":
          document.querySelector('#submitForm').click();
          break;
    
        case "annuler":
          document.querySelector('input[type=file]').value = '';
          break;
    
        default:
          document.querySelector('input[type=file]').value = '';
      }
    });
  }
</script>
@endsection