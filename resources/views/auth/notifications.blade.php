@extends('layouts.sideNavTopNav')

@section('content_1')
<div style="padding: 15px;" class="w-full mx-auto mt-8">
        
    <!-- success alert used by javascript -->
    <div style="display:none;" id="js-alert-success" style="padding: 15px;" class="animated fadeInUp mt-5">
      <div style="padding: 15px;" class="mt-5">
        <p style="padding: 15px;" class="cls-alert"><i class="fas fa-check-circle"></i>
          <span class="ml-2"></span></p>
      </div>
    </div>

    <!-- error alert used by javascript -->
    <div style="display:none;" id="js-alert-error"  style="padding: 15px;" class="animated fadeInUp mt-5">
      <p style="padding: 15px;" class="cls-alert-error">
        <i class="fas fa-times-circle" style=""></i>
        <span class="ml-2"></span></p>
    </div>

    <h1 class="font-normal text-grey-darker text-xl mt-5 mb-8 capitalize px-4" style="">votre notifications</h1>

    <!-- once the user opens this page, all notifications will be marked as read -->
    <?php \Auth::user()->unreadNotifications->markAsRead(); ?>

    @if(\Auth::user()->notifications->count() > 0) 
      @foreach(\Auth::user()->notifications as $notification)
        <div style="padding: 15px;" class="block w-full cls-p-y-0">
          <div style="padding: 15px;" class="relative overflow-hidden  w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">

            <!-- case: user is authenticated and owns this post -->
            <!-- @if(\Auth::check() && \Auth::user()->id == $notification->user_id)
              <i onclick="openSlide(event)" class="fas fa-ellipsis-h text-sm absolute pin-r pin-t m-5 p-1 hover:text-grey-dark" style=""></i>
              <div id="slide" style="display:none;padding: 15px;" class="animated slideInDown faster absolute pin-t pin-x bg-white shadow-lg text-sm text-center">
                  <button onclick="toDeletePage(event, {{ $notification->id }})" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-delete-btn uppercase mx-2">supprimer</button>
                  <button onclick="toUpdatePage(event, {{ $notification->id }})" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-update-btn uppercase mx-1">mettre à jour</button>
                  <i onclick="closeSlide(event)" class="fas fa-times-circle absolute pin-r pin-t m-5 p-1 hover:text-grey-dark" style=""></i>
              </div>
            @endif -->

            <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
              @if(\App\User::whereId($notification->data['user_id'])->first()->accountType == 'person')
              <img id="avatar" src="{{ asset('/storage/'.\App\User::whereId($notification->data['user_id'])->first()->avatar) }}"
                    alt="alt placeholder" style="@if(\App\User::whereId($notification->data['user_id'])->first()->accountType == 'company')padding: 15px !important;@endif" class="rounded cls-p-0 @if(\App\User::whereId($notification->data['user_id'])->first()->accountType != 'company')w-12 @else bg-grey-light @endif h-12 mt-2">
              <span style="padding: 15px;" class="text-sm cls-p-y-0 ">
                {{ \App\User::whereId($notification->data['user_id'])->first()->firstName }} {{ \App\User::whereId($notification->data['user_id'])->first()->lastName }}
              </span>
              @else 
              <img src="{{  asset('/storage/'.\App\User::whereId($notification->data['user_id'])->first()->avatar) }}"
              alt="alt placeholder" style="padding: 15px;" class="cls-post-brand-logo">
              <span style="padding: 15px;" class="text-sm cls-p-y-0 ">
                {{ \App\User::whereId($notification->data['user_id'])->first()->company->companyName }}
              </span>
              @endif
            </div>
            <div style="padding: 15px;" class="cls-p-x-0">
              <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">{{ $notification->data['title'] }}</h1>
            </div>
            
            @if(\App\User::whereId($notification->data['user_id'])->first()->accountType == 'person')
            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
              <i class="fas fa-university text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ \App\User::whereId($notification->data['user_id'])->first()->person->university  }}</span>
              <i class="fas fa-angle-right text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ \App\User::whereId($notification->data['user_id'])->first()->person->category->name  }}</span>          
              <!-- show cv button -->
              <button onclick="showCVModal('{{ asset('storage/cv/users/'.\App\User::whereId($notification->data['user_id'])->first()->id.'.pdf') }}')" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-update-btn uppercase mx-1">voir cv</button>          
              <form id="approvalForm" class="inline" action="{{ route('approve_subscription') }}" method="post">
                @csrf
                <input type="hidden" name="subscription_id" value="{{ $notification->data['subscription_id'] }}">
                <input type="hidden" name="notification_id" value="{{ $notification->id }}">
                <textarea class="hidden" name="message"></textarea>                
                <button onclick="approveSubscription(event)" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-update-btn uppercase mx-1">apprové</button>          
              </form>
            </div>
            @else

            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
              <div style="padding: 15px 0px">
                <i class="fas fa-envelope text-xs mt-1" style=""></i>
                <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2 capitalize">message</span>
              </div>
              <p style="padding: 15px;" class="border rounded w-full">{{ $notification->data['message'] }}</p>
            </div>

            @endif
          </div>
        </div>
      @endforeach
    @else

      <div style="padding: 15px;" class="font-thin text-center text-lg">
        <p>Aucun notifications</p>
      </div>

    @endif

    <div id="CV_Modal" class="w-full h-full fixed pin z-10 hidden" style="background: rgba(0,0,0,0.5);">
      <div style="padding: 15px 15px 55px 15px;" class="animated slideInUp container bg-white mx-auto h-full relative">
        <i onclick="document.querySelector('#CV_Modal').style.display = 'none';" class="fas fa-times-circle hove:bg-black cursor-pointer text-grey-dark absolute pin-r pin-t mr-3 mt-5"></i>
        <div class="w-full bg-black mt-10 h-full">
          <object data="" type="application/pdf" width="100%" class="h-full">
            <p>Alternative text - include a link <a href="myfile.pdf">to the PDF!</a></p>
          </object>
        </div>
      </div>
    </div>

</div>

<script>

function showCVModal(cv_file_path) {
  // show cv modal
  document.querySelector('#CV_Modal').style.display = 'block';
  // assign file path to object 
  document.querySelector('object').setAttribute('data', cv_file_path);
}

function approveSubscription(event) {

  // prevent form submition
  event.preventDefault();

  // prompt cancel, ok 
  swal("if you approve this person for this traineeship, then this offer will be closed automaticly!", {
      buttons: {
        annuler: true,
        oui: true,
      },
    })
    .then((value) => {
        if(value === 'oui') {
          document.querySelector('#approvalForm textarea[name=message]').value = document.querySelector('#approvalMessage').value;
          document.querySelector('#approvalForm').submit();
        }
    })
    const textarea = document.createElement('textarea');
    textarea.setAttribute('class', 'mt-3 w-full h-24 border p-3 rounded');  
    textarea.setAttribute('id', 'approvalMessage');  
    textarea.setAttribute('placeholder', 'message pour votre stagiaire...');  
    document.querySelector('.swal-text').appendChild(textarea);
  }


</script>
@endsection