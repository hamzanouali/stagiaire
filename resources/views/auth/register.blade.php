@extends('layouts.app')
@section('content')
<style>
    html {
        height: 100%;
    }
</style>

<div style="padding: 15px;" class="w-full cls-gradient">
    <p style="padding: 15px;" class="cls-desc-text cls-text-white cls-z-index-3">nous fournissons cette plate-forme afin d'établir une <i class="cls-highlighted cls-hover-before cls-before-transparent" >relation</i> entre <i class="cls-highlighted cls-hover-before cls-before-transparent" >les étudiants</i> et <i class="cls-highlighted cls-hover-before cls-before-transparent" >les entreprises</i></p><img src="{{ asset('storage/logo.png') }}" alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24 mt-5 ml-3 cls-z-index-3 cls-small-mobile"></div>
        <div style="padding: 15px;" class="bg-white w-full lg:w-2/3 relative cls-form-container"><!-- Create new account -> -->
    
    <div onclick="loading.start();window.location.href= '/login'" class="absolute pin-r pin-t text-grey-darkest cls-absolute-div" >
        <a  class="cls-border-hover cursor-pointer">{{ __('Login') }}</a>
        <i class="fas fa-long-arrow-alt-right ml-1" ></i>
    </div>
    <!-- Form -->
    <form onsubmit="loading.start()" class="w-full p-0" method="POST" action="{{ route('register') }}">
        @csrf
        <h1 class="font-normal text-xl mt-5 mb-8 w-full mx-5 text-grey-darker capitalize" >{{ __('Register') }}</h1>
        <div class="w-full  p-5 pt-0 ">
            <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
            <input name="firstName" type="text" required class="w-full font-normal px-4 py-3 bg-white border rounded @error('firstName') cls-border-red @enderror" value="{{ old('firstName') }}" required autocomplete="name" placeholder="{{ __('Votre Nom') }}" >
            @error('firstName')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>
        <div class="w-full  p-5 pt-0 ">
            <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
            <input name="lastName" type="text" required class="w-full font-normal px-4 py-3 bg-white border rounded @error('lastName') cls-border-red @enderror" value="{{ old('lastName') }}" required autocomplete="name" placeholder="{{ __('Votre Prénom') }}" >
            @error('lastName')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>
        <div class="w-full  p-5 pt-0 ">
            <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
            <input name="email" type="email" required class="w-full font-normal px-4 py-3 bg-white border rounded @error('email') cls-border-red @enderror" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}" >
            @error('email')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>
        <div class="w-full  p-5 pt-0 ">
            <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
            <select name="accountType" value="{{ old('accountType') }}" required class="w-full font-normal px-4 py-3 capitalize bg-white border rounded @error('accountType') cls-border-red @enderror">
              <option value="" selected=""  class="text-grey"> Choisir une catégorie </option>
              <option value="company"> entreprise </option>
              <option value="person"> étudiante </option>
            </select>
            @error('accountType')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>
        <div class="w-full  p-5 pt-0 ">
            <input name="password" type="password" required class="w-full font-normal px-4 py-3 bg-white border rounded @error('password') cls-border-red @enderror" placeholder="{{ __('Password') }}" >
            @error('password')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>

        <div class="w-full  p-5 pt-0 ">
            <input name="password_confirmation" type="password" required class="w-full font-normal px-4 py-3 bg-white border rounded @error('password_confirmation') cls-border-red @enderror" placeholder="{{ __('Confirm Password') }}" >
        </div>
        <div class="w-full p-5 items-start" >
            <button class="px-8 py-3 cls-btn-primary uppercase cursor-pointer" >{{ __('Register') }}</button>
        </div>
    </form>
</div>
@endsection