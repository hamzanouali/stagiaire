@extends('layouts.app')
@section('content')
<section id="main-body" class="cls-main-body w-full h-full flex flex-row-reverse flex-wrap lg:flex-no-wrap" style="">
    <div style="padding: 15px;" class="cls-app-side hidden lg:block animated slideInLeft w-1/5 cls-gradient cls-profile-side-nav">
      <div style="padding: 15px;" class="flex items-start mb-16 cls-p-x-0 cls-p-y-0">
        <img src="https://uploads-ssl.webflow.com/5bcb46130508ef456a7b2930/5c7121203f8bb20f5623ca81_invision-logo-export-v1.png"
          alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24 cls-z-index-3 cls-small-mobile">
      </div>
      <div style="padding: 15px;" class="capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-pen text-xs mt-1" style=""></i>
        <span style="padding: 15px;" class="">Editer le profil</span>
      </div>
      <div style="padding: 15px;" class="capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-list text-xs mt-1" style=""></i>
        <span style="padding: 15px;" class="">mes stages</span>
      </div>
      <div style="padding: 15px;" class="capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-sign-out-alt text-xs mt-1" style=""></i>
        <span style="padding: 15px;" class="">Déconnexion</span>
      </div>
      <div style="padding: 15px;" class="cls-letter-space-px uppercase text-xs leading-normal mt-5 cls-p-x-0">
        <span style="padding: 15px;" class="cls-p-0">tous les droits sont réservés à Hamza nouali et omar ourak</span>
      </div>
    </div>
    <div style="padding: 15px;" class="w-full lg:w-4/5 cls-form-container cls-p-0">
      <!-- Form -->
      <div style="padding: 15px;" class="border-b shadow relative bg-white flex sticky pin-t z-10">
        <i class="fas fa-th ml-5 leading-normal p-2 absolute pin-r pin-t mr-8 mt-6" style=""></i>
        <div style="padding: 15px;" class="ml-4 w-48 flex items-start cls-p-x-0 cls-p-y-0 overflow-hidden">
          <div style="padding: 15px;" class="cls-side-nav-img-container cls-p-y-0">
            <img src="https://images.unsplash.com/photo-1505503693641-1926193e8d57?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=334&amp;q=80"
              alt="alt placeholder" style="padding: 15px;" class="rounded cls-p-0 w-12 h-12 mt-2">
          </div>
          <div style="padding: 15px;" class="w-3/5 cls-p-x-0 cls-p-y-0 ml-2">
            <span style="padding: 15px;" class="cls-p-0 capitalize block w-full mt-2 text-sm">Abdelhak Jhony Deep</span>
          </div>
        </div>
      </div>
      <div style="padding: 15px;" class="w-full mx-auto mt-8 flex flex-wrap items-start">
        <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">votre profile</h1>
        <div style="padding: 15px;" class="w-full lg:w-1/2 cls-p-y-0">
          <div style="padding: 15px;" class="w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">
            <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png"
                alt="alt placeholder" style="padding: 15px;" class="cls-post-brand-logo">
              <span style="padding: 15px;" class="text-sm cls-p-y-0 ">par Google.inc</span>
              <i class="far fa-clock text-xs mt-1" style=""></i>
              <i style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">28 min plustard</i>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0">
              <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">stage de developement web - full stack (pHP, javaScript, HTML, CSS)+ Base de donnés MySQL</h1>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
              <i class="fas fa-map-marker-alt text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">Alger</span>
              <i class="far fa-user-circle text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">50 places</span>
            </div>
          </div>
        </div>
        <div style="padding: 15px;" class="w-full lg:w-1/2 cls-p-y-0">
          <div style="padding: 15px;" class="w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">
            <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png"
                alt="alt placeholder" style="padding: 15px;" class="cls-post-brand-logo">
              <span style="padding: 15px;" class="text-sm cls-p-y-0 ">par Google.inc</span>
              <i class="far fa-clock text-xs mt-1" style=""></i>
              <i style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">28 min plustard</i>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0">
              <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">stage de developement web - full stack (pHP, javaScript, HTML, CSS)+ Base de donnés MySQL</h1>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
              <i class="fas fa-map-marker-alt text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">Alger</span>
              <i class="far fa-user-circle text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">50 places</span>
            </div>
          </div>
        </div>
        <div style="padding: 15px;" class="w-full lg:w-1/2 cls-p-y-0">
          <div style="padding: 15px;" class="w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">
            <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png"
                alt="alt placeholder" style="padding: 15px;" class="cls-post-brand-logo">
              <span style="padding: 15px;" class="text-sm cls-p-y-0 ">par Google.inc</span>
              <i class="far fa-clock text-xs mt-1" style=""></i>
              <i style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">28 min plustard</i>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0">
              <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">stage de developement web - full stack (pHP, javaScript, HTML, CSS)+ Base de donnés MySQL</h1>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
              <i class="fas fa-map-marker-alt text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">Alger</span>
              <i class="far fa-user-circle text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">50 places</span>
            </div>
          </div>
        </div>
        <div style="padding: 15px;" class="w-full lg:w-1/2 cls-p-y-0">
          <div style="padding: 15px;" class="w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">
            <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/1200px-Google_2015_logo.svg.png"
                alt="alt placeholder" style="padding: 15px;" class="cls-post-brand-logo">
              <span style="padding: 15px;" class="text-sm cls-p-y-0 ">par Google.inc</span>
              <i class="far fa-clock text-xs mt-1" style=""></i>
              <i style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">28 min plustard</i>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0">
              <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">stage de developement web - full stack (pHP, javaScript, HTML, CSS)+ Base de donnés MySQL</h1>
            </div>
            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
              <i class="fas fa-map-marker-alt text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">Alger</span>
              <i class="far fa-user-circle text-xs mt-1" style=""></i>
              <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">50 places</span>
            </div>
          </div>
        </div>
        <div style="padding: 15px;" class="cls-p-x-0 w-full">
          <i class="cls-pagination-btn fas fa-angle-double-left" style=""></i>
          <span style="padding: 15px;" class="cls-pagination-btn">1</span>
          <span style="padding: 15px;" class="cls-pagination-btn">2</span>
          <span style="padding: 15px;" class="cls-pagination-btn">3</span>
          <i class="cls-pagination-btn fas fa-angle-double-right" style=""></i>
        </div>
      </div>
    </div>
  </section>
@endsection