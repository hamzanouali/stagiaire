@extends('layouts.app')
@section('content')
<style>
    html {
        height: 100%;
    }
</style>

<div style="padding: 15px;" class="w-full cls-gradient">
    <p style="padding: 15px;" class="cls-desc-text cls-text-white cls-z-index-3">nous fournissons cette plate-forme afin d'établir une <i class="cls-highlighted cls-hover-before cls-before-transparent" >relation</i> entre <i class="cls-highlighted cls-hover-before cls-before-transparent" >les étudiants</i> et <i class="cls-highlighted cls-hover-before cls-before-transparent" >les entreprises</i></p><img src="{{ asset('storage/logo.png') }}" alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24 mt-5 ml-3 cls-z-index-3 cls-small-mobile"></div>
        
        <div style="padding: 15px;" class="bg-white w-full lg:w-2/3 relative cls-form-container"><!-- Create new account -> -->
    
    <div onclick="loading.start();window.location.href= '/register'" class="absolute pin-r pin-t text-grey-darkest cls-absolute-div" >
        <a  class="cls-border-hover cursor-pointer">{{ __("créer un nouveau compte") }}</a>
        <i class="fas fa-long-arrow-alt-right ml-1" ></i>
    </div>
    <!-- Form -->
    <form onsubmit="loading.start()" class="w-full p-0" method="POST" action="{{ route('login') }}">
        @csrf
        <h1 class="font-normal text-xl mt-5 mb-8 w-full mx-5 text-grey-darker capitalize" >{{ __('Login') }}</h1>
        <div class="w-full  p-5 pt-0 ">
            <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
            <input name="email" type="email" class="w-full font-normal px-4 py-3 bg-white border rounded @error('email') cls-border-red @enderror" value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('E-Mail Address') }}" >
            @error('email')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>
        <div class="w-full  p-5 pt-0 ">
            <input name="password" type="password" class="w-full font-normal px-4 py-3 bg-white border rounded @error('password') cls-border-red @enderror" placeholder="{{ __('Password') }}" >
            @error('password')
                <!--this div contains a span which contain error message-->
                <div class="w-full py-1 px-1 text-red text-sm">
                    <span class="cls-red" >{{ $message }}</span>
                </div>
            @enderror
        </div>
        <!--Keep me logged in checkbox-->
        <div onclick="checkRememberMe()" class="w-full relative px-8 py-2 text-sm text-grey-darker cls-remember-me" >
            @if(old('remember'))
                <i class="far fa-check-square text-grey-dark" ></i>
            @else
                <i class="far fa-square text-grey-dark" ></i>
            @endif
            <span class="ml-2 cursor-pointer" >{{ __('Remember Me') }}</span>
            <input class="absolute pin w-full h-full invisible" type="checkbox" name="remember" id="remember" value="{{ old('remember') ? 'checked' : '' }}">
            <script>
                function checkRememberMe() {
                    const element = document.querySelector('.fa-square')
                    if(element) {
                        element.className = element.className.replace('fa-square', 'fa-check-square')
                    } else {
                        const element = document.querySelector('.fa-check-square')
                        element.className = element.className.replace('fa-check-square', 'fa-square')                        
                    }
                }
            </script>
        </div>
        <div class="w-full p-5 items-start" >
            <button class="px-8 py-3 cls-btn-primary uppercase cursor-pointer" >{{ __('suivant') }}</button>
        </div>
    </form>
    @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}" class="block w-full px-8 py-2 text-sm text-grey-darker cls-remember-me cursor-pointer" >
            {{ __('Forgot Your Password?') }}
        </a>
    @endif
</div>
@endsection