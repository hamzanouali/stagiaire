@extends('layouts.sideNavTopNav')

@section('content_1')
<div style="padding: 15px;" class="w-full mx-auto mt-8">
        
    <!-- success alert used by javascript -->
    <div style="display:none;" id="js-alert-success" style="padding: 15px;" class="animated fadeInUp mt-5">
      <div style="padding: 15px;" class="mt-5">
        <p style="padding: 15px;" class="cls-alert"><i class="fas fa-check-circle"></i>
          <span class="ml-2"></span></p>
      </div>
    </div>

    @error('error_message')
    <!-- error alert used by javascript -->
    <div id="js-alert-error"  style="padding: 15px;" class="animated fadeInUp mt-5">
      <p style="padding: 15px;" class="cls-alert-error">
        <i class="fas fa-times-circle" style=""></i>
        <span class="ml-2">{{ $message }}</span></p>
    </div>
    @enderror

    <!-- change image alert -->
    <div style="padding: 15px;" class="mt-5">
      <p style="padding: 15px;" class="cls-alert-info">
        <i class="far fa-lightbulb" style=""></i>
        <span class="ml-2" style="">vous voudrez peut-être changer l'image de votre profil..</span>
        <button onclick="document.querySelector('#ModalApp').style.display = 'block'" class="ml-2"> changer </button>
      </p>
    </div>

    <form onsubmit="loading.start()" class="w-full p-0 flex flex-wrap" method="post" action="{{ route('update_profile') }}">
      @csrf

      <h1 class="font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize">votre profile</h1>
      <div class="w-full lg:w-1/2  p-5 pt-0 ">
        <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
        <div style="padding: 15px;" class="cls-p-x-0 mb-2">
          <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">nom</span>
        </div>
        <input value="{{ $user->lastName }}" name="lastName" type="text" class="w-full font-normal px-4 py-3 bg-white border capitalize rounded @error('lastName') cls-border-red @enderror" placeholder="Example"
          >
        @error('lastName')
        <!--this div contains a span which contain error message-->
        <div class="w-full py-1 px-1 text-red text-sm">
          <span class="cls-red">{{ $message }}</span>
        </div>
        @enderror
      </div>
      <div class="w-full lg:w-1/2  p-5 pt-0 ">
        <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
        <div style="padding: 15px;" class="cls-p-x-0 mb-2">
          <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">prenom</span>
        </div>
        <!--this div contains a span which contain error message-->
        <input value="{{ $user->firstName }}" name="firstName" type="text" class="w-full font-normal px-4 py-3 bg-white border capitalize rounded @error('firstName') cls-border-red @enderror" placeholder="Example"
          >
        @error('firstName')
        <!--this div contains a span which contain error message-->
        <div class="w-full py-1 px-1 text-red text-sm">
          <span class="cls-red">{{ $message }}</span>
        </div>
        @enderror
      </div>
      <div class="w-full lg:w-1/2  p-5 pt-0 ">
        <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
        <div style="padding: 15px;" class="cls-p-x-0 mb-2">
          <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">email</span>
        </div>
        <input value="{{ $user->email }}" name="email"  type="text" class="w-full font-normal px-4 py-3 bg-white border rounded @error('email') cls-border-red @enderror" placeholder="email@email.com"
          >
        @error('email')
        <!--this div contains a span which contain error message-->
        <div class="w-full py-1 px-1 text-red text-sm">
          <span class="cls-red">{{ $message }}</span>
        </div>
        @enderror
      </div>

      <!--Keep me logged in checkbox-->
      <div class="w-full lg:w-1/2  p-5 pt-0 ">
        <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
        <div style="padding: 15px;" class="cls-p-x-0 mb-2">
          <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">adresse</span>
        </div>
        <input name="address" value="{{ $user->address }}"  type="text" class="w-full font-normal px-4 py-3 bg-white border capitalize rounded @error('address') cls-border-red @enderror" placeholder="Wilaya, dayra.."
          >
        @error('address')
        <!--this div contains a span which contain error message-->
        <div class="w-full py-1 px-1 text-red text-sm">
          <span class="cls-red">{{ $message }}</span>
        </div>
        @enderror
      </div>
      <div class="w-full p-5 pt-0 ">
        <div style="padding: 15px;" class="cls-p-x-0 mb-2">
          <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">type de compte</span>
        </div>
        <input style="background:#f1f5f7;" disabled value="{{ $user->accountType }}" type="text" class="w-full font-normal cursor-not-allowed px-4 py-3 bg-white border capitalize rounded @error('accountType') cls-border-red @enderror">
        <input name="accountType" value="{{ $user->accountType }}" type="text" class="hidden">
          @error('accountType')
        <!--this div contains a span which contain error message-->
        <div class="w-full py-1 px-1 text-red text-sm">
          <span class="cls-red">{{ $message }}</span>
        </div>
        @enderror
      </div>
      
      <!-- person inputs -->
      @if($user->accountType === 'person')
      <div id="person" class="w-full cls-p-x-0 cls-p-y-0 flex flex-wrap animated fadeInUp">
        <div class="w-full lg:w-1/2  p-5 pt-0 ">
          <div style="padding: 15px;" class="cls-p-x-0 mb-2">
            <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">votre spécialité</span>
          </div>
          <select name="category_id" class="w-full font-normal px-4 py-3 bg-white border rounded @error('category_id') cls-border-red @enderror">
            <option value=""  class="text-grey"> Choisir une spécialité </option>
            @foreach($categories as $category)
              <option @if($user->category_id == $category->id) selected="" @endif value="{{ $category->id }}"> {{ $category->name }} </option>
            @endforeach
          </select>
          @error('category_id')
          <!--this div contains a span which contain error message-->
          <div class="w-full py-1 px-1 text-red text-sm">
            <span class="cls-red">{{ $message }}</span>
          </div>
          @enderror
        </div>
        <div class="w-full lg:w-1/2  p-5 pt-0 ">
          <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
          <div style="padding: 15px;" class="cls-p-x-0 mb-2">
            <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">université</span>
          </div>
          <input name="university" value="{{ $user->university }}" type="text" class="w-full font-normal px-4 py-3 bg-white border capitalize rounded @error('university') cls-border-red @enderror" placeholder="Example: Blida 1"
          >
          @error('university')
          <!--this div contains a span which contain error message-->
          <div class="w-full py-1 px-1 text-red text-sm">
            <span class="cls-red">{{ $message }}</span>
          </div>
          @enderror
        </div>
      </div>
      @endif

      <!-- company inputs -->
      @if($user->accountType === 'company')
      <div id="company" class="w-full cls-p-x-0 cls-p-y-0 flex flex-wrap animated fadeInUp">
        <div class="w-full lg:w-1/2  p-5 pt-0 ">
          <!--check the class, 'border-red' must be there in case of error, otherwise remove it-->
          <div style="padding: 15px;" class="cls-p-x-0 mb-2">
            <span style="padding: 15px;" class="cls-p-x-0 cls-title-label">Nom de la compagnie</span>
          </div>
          <input name="companyName" value="{{ $user->companyName }}" type="text" class="w-full font-normal px-4 py-3 bg-white border capitalize rounded @error('companyName') cls-border-red @enderror" placeholder="somthing"
          >
          @error('companyName')
          <!--this div contains a span which contain error message-->
          <div class="w-full py-1 px-1 text-red text-sm">
            <span class="cls-red">{{ $message }}</span>
          </div>
          @enderror
        </div>
      </div>
      @endif
      
      <div class="w-full p-5 items-start">
        <button class="px-8 py-3 cls-btn-primary cursor-pointer">enregistrer</button>
      </div>
    </form>
  </div>
  <!-- Modal -->
  <div id="ModalApp" style="display:none" style="padding: 15px;" class="fixed pin z-10 cls-modal" style="padding-bottom: 30px; padding-top: 30px;">
    <div style="padding: 15px;" class="animated fadeInUp w-full lg:w-1/3 mx-auto rounded bg-white shadow-lg h-full lg:h-auto overflow-y-auto">
      <h1 class="font-normal text-grey-darker text-xl mt-5 mb-8 capitalize px-4" style="">votre profile</h1>

      <!-- Content goes here -->
      <div style="padding: 15px;" class="">

        <div class="Croppie_Component_Root">
  
          <!-- drag and drop container --> 
          <div id="draggContainer" draggable="true" class="w-full font-thin h-48 text-center p-12 border text-2xl rounded leading-normal text-grey-darker uppercase bg-grey-lightest relative">
            cliquez ou glissez et déposez votre image ici
            <input type="file" @change="coppie($event)" class="absolute pin w-full h-full opacity-0">
          </div>

          <!-- croppie container -->
          <div id="demo-basic" class="hidden">
            <button @click="change()" class="bg-grey-lightest mb-3 mr-3 rounded text-sm px-5 py-2 text-grey-darker">
              <span>changer</span>
              <i class="fas fa-retweet ml-3"></i>
            </button>
            <button @click="rotate()" class="bg-grey-lightest mb-3 mr-3 rounded text-sm px-5 py-2 text-grey-darker">
              <i class="fas fa-redo"></i>
            </button>
          </div>

          <!-- logo case -->
          <div>
            <img id="image-show" src="" alt="">
          </div>

        </div>

      </div>
      <div class="w-full p-5 items-start text-xs text-center" style="">
        <button @click="saveUserAvatar()" class="px-8 py-3 cls-btn-primary cursor-pointer mx-1" style="">ok</button>
        <button onclick="document.querySelector('#ModalApp').style.display = 'none'" class="px-8 py-3 cls-btn-primary cls-cancel-btn cursor-pointer mx-1" style="">annuler</button>
      </div>
    </div>

  </div>

  <script>
  var app = new Vue({
    el: '#ModalApp',
    data() {
      return {
        croppie: null,

        degree: 0
      }
    },

    mounted() {
      /**
      * init Croppie.js
      */
      this.croppie = new Croppie(document.querySelector("#demo-basic"), {
        viewport: {
          width: 200,
          height: 200,
          type: "square"
        },
        boundary: {
          height: 300
        },
        enableOrientation: true
      });

      /**
      * emit change onUpdate
      */
      document.getElementById("demo-basic").addEventListener("update", e => {
        /**
        ** get cropped image as base64 format
        */
        this.croppie.result("base64").then(result => {
          /**
          ** assign result to images
          */
          document.querySelectorAll("#avatar").forEach(element => {
            element.setAttribute("src", result);
          });
        });
      });
    },
    methods: {
      saveUserAvatar() {
        loading.start();

        // Make a request for a user with a given ID
        axios.post('/profile/avatar/update', {
          avatar: document.querySelector('#avatar').getAttribute('src')
        })
          .then(response => {
            if(response.status === 200) {
              loading.stop();
              /**
              * hide the modal
              */
              document.querySelector('#ModalApp').style.display = 'none'
              /**
              * show success alert
              */
              const alert = document.querySelector('#js-alert-success')
              alert.querySelector('span').textContent = 'votre image a été mise à jour avec succès.'
              alert.style.display = 'block'
            }
          })
          .catch(function (error) {
            loading.stop();
            /**
              * hide the modal
              */
              document.querySelector('#ModalApp').style.display = 'none'
            /**
              * show success alert
              */
            const alert = document.querySelector('#js-alert-error')
            alert.querySelector('span').textContent = "quelque chose s'est mal passé, nous n'avons pas pu mettre à jour votre image"
            alert.style.display = 'block'
          });
      },

      rotate() {
        this.croppie.rotate(this.degree + 90);
      },

      assignLogo() {
        if (event.target.files && event.target.files[0]) {
          var reader = new FileReader();
          reader.onload = e => {
            document.querySelector('#avatar').setAttribute('src', e.target.result)
            document.querySelector('#image-show').setAttribute('src', e.target.result)
          };
          reader.readAsDataURL(event.target.files[0]);
        }
      },

      /**
      ** init croppie.js
      */
      coppie(event) {

        /**
        * in case user upload logo
        * so there is no need for croppie
        */
        if(document.querySelector('input[name="accountType"]').value === 'company') {
          return this.assignLogo();
        } 

        /**
        ** hide draggContainer
        */
        this.hide("draggContainer");
        /**
        ** show basic-demo
        */
        this.show("demo-basic");

        if (event.target.files && event.target.files[0]) {
          var reader = new FileReader();
          reader.onload = e => {
            this.croppie.bind({
              url: e.target.result
            });
          };
          reader.readAsDataURL(event.target.files[0]);
        }
      },
      /**
      ** show draggContainer and hide croppie-container
      */
      change() {
        this.show("draggContainer");
        this.hide("demo-basic");
      },
      /**
      ** hide element by giving it hidden className
      */
      hide(id) {
        document.getElementById(id).className += " hidden";
      },
      /**
      ** show element removing hidden className
      */
      show(id) {
        document.getElementById(id).className = document
          .getElementById(id)
          .className.replace("hidden", " ");
      }
    }
  });
</script>
@endsection