<!--navigation starts here-->
  <div style="padding: 15px;" class="border-b shadow relative bg-white flex sticky pin-t cls-z-1">
    <div style="padding: 15px;" class="container mx-auto relative cls-p-y-0 cls-p-x-0">
      <img onclick="window.location.href = window.location.origin + '/traineeships'" src="{{ asset('storage/logo.png') }}" alt="alt placeholder" style="padding: 15px;" class="cls-logo cursor-pointer h-24 cls-not-bright ml-3 lg:ml-5">
<!--       <i class="fas fa-envelope leading-normal p-2 border border-red-light text-red-light absolute pin-r pin-t mr-16 cls-p-x-0" style="
          width: 40px;
          border-radius: 50px;
          text-align: center;
          "> <span class="absolute text-white pin-b pin-l bg-red-light rounded-full font-normal -mb-4 -ml-4 w-8 h-8 py-2 text-center text-sm">83</span> </i>
 -->
      @if(\Auth::check())

        <!-- case 1: we user have not readed notifications -->
        @if(count(\Auth::user()->unreadNotifications) > 0)
          <i onclick="window.location.href = window.location.origin + '/profile/notifications'" class="cursor-pointer fas fa-bell leading-normal p-2 border border-red-light text-red-light absolute pin-r pin-t mr-3 cls-p-x-0" style="
          width: 40px;
          border-radius: 50px;
          text-align: center;
          "> <span class="absolute text-white pin-b pin-l bg-red-light rounded-full font-normal -mb-4 -ml-4 w-8 h-8 py-2 text-center text-sm">{{ count(\Auth::user()->unreadNotifications) }}</span> </i>
        @else 
          <i onclick="window.location.href = window.location.origin + '/profile/notifications'" class="cursor-pointer fas fa-bell ml-5 leading-normal p-2 absolute pin-r pin-t mr-3 cls-p-x-0" style="
              border: 1px solid #ccc;
              width: 40px;
              border-radius: 50px;
              text-align: center;
          "></i>
        @endif

      <!-- guest user (not authenticated) -->
      @else
      <a href="/login" class="cls-btn px-4 pin-r pin-t py-2 mt-1 mr-4 cls-btn-primary float-right">Login</a>
      @endif    
      
    </div>

</div>