<div id="filterExecuter" style="display:none" class="animated fadeInUp faster bg-white shadow-lg w-full fixed text-center h-24 p-5 pin-b pin-x z-40">
    <button class="mr-4 bg-grey-light text-black px-8 h-full rounded-sm">Annuler</button>
    <button onclick="filter()" class="bg-blue text-white px-8 h-full rounded-sm">Filtrer</button>

    <script>
      /**
       * prepares filter URL location & category and redirect to filter link
       *
       */
      function filter() {
        const element = document.querySelector('#filterExecuter');
        
        let link = window.location.origin + '/traineeships/filter';

        if(element.hasAttribute('location')) {
          link += '?location=' + element.getAttribute('location'); 
        } else {
          link += '?location=all'; 
        }

        if(element.hasAttribute('categories')) {
          link += '&category=' + element.getAttribute('categories'); 
        } else {
          link += '&category=all'; 
        }

        // redirect
        window.location.href = link;

      }
    </script>
</div>

<!-- Filter -->
<div id="{{ $filterName }}App" class="w-full relative" style="padding: 15px;">

  <div style="padding: 15px;" class="w-full bg-black rounded mb-2 cls-filter-container">
    <div style="padding: 15px;" class="mb-5 capitalize cls-filter-container text-lg">
      <i class="{{ $filterIcon }} -ml-3" style=""></i>
      <span class="ml-3" style="">{{ $filterName }}</span>
    </div>
    
    <checkbox @input="filterList = $event" list="{{ json_encode($list) }}"></checkbox>

  </div>

  

</div>

<script>
  checkbox = {
    template: `<div>
      <div class="w-full border-b cls-filter-li"></div>
      <div :ref="'checkbox_'+item.id" @click="checkIt(item.id)" :key="item.id" v-for="item in list_1"  style="padding: 15px;" class="border-b border-l border-r cls-filter-li cursor-pointer hover:text-white">
        <i class="far fa-square  mt-1" style=""></i>
        <span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-5 capitalize">@{{ item.name }}</span>
      </div>
    </div>`,

    props: {
      list: {
        required: true
      }
    },

    data() {
      return {
        list_1: JSON.parse(this.list),
        
        checkedList: []
      }
    },

    mounted() {
      /**
       * in case we are in filter page
       * get selected categories from url
       * then push it to checkbox component to be checked
       */
      @if(strpos($_SERVER['REQUEST_URI'], 'filter'))
        let checked = '{{ app('request')->category }}';
        checked.split(',').forEach(element => {
          
          this.checkIt(Number(element));
        });
      @endif
    },

    methods: {
      checkIt(index) {
        // indexOf does not search for objects
        // so this will get the index of an object
        let indexWhereId = (index) => {
          for (let i = 0; i < this.list_1.length; i++) {
            const element = this.list_1[i];
            if(element.id === index) return i;
          }
        };
        const parent = this.$refs[`checkbox_${index}`][0]
        const iTag = parent.childNodes[0]

        // exists, then remove it
        if (this.checkedList.includes(this.list_1[indexWhereId(index)].id)) {
          parent.className = parent.className.replace(' cls-filter-checked ', '')
          iTag.className = 'far fa-square'
          const indexFromCheckedList = this.checkedList.indexOf(
            this.list_1[indexWhereId(index)].id
          )
          this.checkedList.splice(indexFromCheckedList, 1)
        } else {
          parent.className += ' cls-filter-checked '
          iTag.className = 'fas fa-check-square'
          this.checkedList.push(this.list_1[indexWhereId(index)].id)
        }

        this.$emit('input', this.checkedList);

      }
    }
  };

  app = new Vue({
    el: '#{{ $filterName }}App',
    components: {
      checkbox
    },
    data() {
      return {
        filterList: [],
      }
    },

    watch: {
      filterList(newVal, oldVal) {
        const element = document.querySelector('#filterExecuter');
        if(this.filterList.length) {
          element.style.display = 'block';
          element.setAttribute('categories', this.filterList.join(','));
        } else {
          element.style.display = 'none';
          element.setAttribute('categories', '');          
        }
        
      }
    },

    methods: {
      excuteFilter() {
        window.location.href = window.location.origin + '/traineeships/filter?f=test&{{ $filter }}=' + this.filterList.join(',');
      }
    }
  });

</script>