<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ asset('assets/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/tailwindcss/dist/tailwind.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/style.css') }}" rel="stylesheet">

        <!-- animate.css -->
        <link rel="stylesheet" href="{{ asset('node_modules/animate.css/animate.min.css') }}">

        <!-- Axios -->
        <script src="{{ asset('node_modules/axios/dist/axios.min.js') }}"></script>

        <!-- sweetalert -->
        <script src="{{ asset('assets/sweetalert/dist/sweetalert.min.js') }}"></script>

        <!-- Croppie.js -->
        <link rel="stylesheet" href="{{ asset('node_modules/croppie/croppie.css') }}">
        <script
        src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('node_modules/croppie/croppie.min.js') }}"></script>
        

        <!-- development version, includes helpful console warnings -->
        <script src="{{ asset('node_modules/vue/dist/vue.js') }}"></script>

        <title>{{ config('app.name', 'Laravel') }}</title>

        <style>
            a {
                text-decoration: none !important;
                color: inherit;
            }

            body {
                height: auto;
            }
        </style>
    </head> 
    <body style="height: 100% !important;" class="cls-main-body cls-grey-body w-full h-full lg:h-screen flex flex-wrap lg:flex-no-wrap">
     
        @yield('content')

        <!-- Loading -->
        <div id="loading" style="display:none;transition: 0.2s;" class="fixed pin z-10 cls-loading-modal cls-px-0">
            <div class="animated fadeInDown faster w-full mx-auto p-12 font-thin bg-white shadow-lg mt-8 lg:mt-16 text-center cls-loading-modal-div">
                <div class="loader">
                    <svg class="circular" viewBox="25 25 50 50" style="">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                    </svg>
                </div>
                <p style="padding: 15px;" class="mt-5">Patientez s'il vous plait...</p>
            </div>
        </div>
        <!-- for toggling loading -->
        <script>
            const loadingElement = document.querySelector('#loading')

            window.loading.start =  () => {
                loadingElement.style.display = 'block'
            }

            window.loading.stop =  () => {
                loadingElement.style.display = 'none'
            }
        </script>

    </body>
</html>
