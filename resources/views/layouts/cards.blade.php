
<!-- 
cases: 
  case 1: we are in index page, $traineeships contains an array
  case 2: we are in show page, $traineeships is an array contains one item
 -->  
@if(count($traineeships) == 0) 
<div class="text-center text-3xl font-thin capitalize">
  aucun stage trouvé
</div>
@endif

@foreach($traineeships as $traineeship)

      <a href="/traineeships/show/{{ $traineeship->id }}" id="tr_{{ $traineeship->id }}" onclick="loading.start()" style="padding: 15px;" class="block w-full @if(strpos($_SERVER['REQUEST_URI'], 'profile')) lg:w-1/2  @endif cls-p-y-0">
        <div style="padding: 15px;" class="relative overflow-hidden  w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">

          <!-- case: user is authenticated and owns this post -->
          @if(\Auth::check() && \Auth::user()->id == $traineeship->company->user_id)
            <i onclick="openSlide(event)" class="fas fa-ellipsis-h text-sm absolute pin-r pin-t m-5 p-1 hover:text-grey-dark" style=""></i>
            <div id="slide" style="display:none;padding: 15px;" class="animated slideInDown faster absolute pin-t pin-x bg-white shadow-lg text-sm text-center">
                <button onclick="toDeletePage(event, {{ $traineeship->id }})" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-delete-btn uppercase mx-2">supprimer</button>
                <button onclick="toUpdatePage(event, {{ $traineeship->id }})" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-update-btn uppercase mx-1">mettre à jour</button>
                <i onclick="closeSlide(event)" class="fas fa-times-circle absolute pin-r pin-t m-5 p-1 hover:text-grey-dark" style=""></i>
            </div>
          @endif

          <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
            <img src="{{ asset('storage/users') }}/{{ $traineeship->company->user_id }}.png"
              alt="alt placeholder" style="padding: 0px;" class="h-8 w-auto">
            <span style="padding: 15px;" class="text-sm cls-p-y-0 ">{{ $traineeship->company->companyName }}</span>
            <i class="far fa-clock text-sm mt-1" style=""></i>
            <i style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">éxpiration {{ date('Y-m-d', strtotime($traineeship->lastDay)) }}</i>
          </div>
          <div style="padding: 15px;" class="cls-p-x-0">
            <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">{{ $traineeship->title }}</h1>
          </div>
          <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
            <i class="fas fa-map-marker-alt text-xs mt-1" style=""></i>
            <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ $traineeship->location }}</span>
            <i class="fas fa-folder text-xs mt-1" style=""></i>
            <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ $traineeship->category->name }}</span>
          
          </div>
          
          <!-- case: we are in /traineeships/id -->

          @if (strpos($_SERVER['REQUEST_URI'], 'show'))
            <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0 mt-5">
              <p style="padding: 15px;" class="cls-p-x-0"> {{ $traineeship->description }} </p>
            </div>
            @if(\Auth::check() && \Auth::user()->accountType == 'person')

              @if($traineeship->subscription != null 
              && 
              $traineeship->subscription->person->user_id == \Auth::user()->id)
                <form onsubmit="loading.start()" action="/subscribe/delete/{{ $traineeship->id }}" method="post" style="padding: 15px;" class="text-center">
                  @csrf
                  <input type="hidden" name="traineeship_id" value="{{ $traineeship->id }}">
                  <button class="px-8 py-3 cls-btn-primary cls-cancel-btn cursor-pointer" style="">annuler</button>
                </form>
              @else 
                <form onsubmit="loading.start()" action="/subscribe/create" method="post" style="padding: 15px;" class="text-center">
                  @csrf
                  <input type="hidden" name="traineeship_id" value="{{ $traineeship->id }}">
                  <button class="px-8 py-3 cls-btn-primary cursor-pointer" style="">souscrire</button>
                </form>
              @endif

            @else 
              <!-- <div onclick="alert('you are not loged in');" style="padding: 15px;" class="text-center">
                <span class="px-8 py-3 cls-btn-primary cursor-pointer" style="">souscrire</span>
              </div> -->
            @endif
          @endif

        </div>
      </a>

    @endforeach