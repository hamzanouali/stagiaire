@extends('layouts.app')
@section('content')
<section id="main-body" class="cls-main-body w-full h-full flex flex-row-reverse flex-wrap lg:flex-no-wrap">
    <div style="padding: 15px;" class="cls-app-side hidden lg:block animated slideInLeft w-1/5 cls-gradient cls-profile-side-nav">
      <div style="padding: 15px;" class="flex items-start mb-16 cls-p-x-0 cls-p-y-0">
        <img onclick="loading.start();window.location.href = window.location.origin + '/traineeships'" src="{{ asset('storage/logo.png') }}"
          alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24 cls-z-index-3 cls-small-mobile cursor-pointer">
      </div>

      @if(\Auth::user()->accountType === 'company')
      <a href="/profile/traineeship/create" onclick="loading.start()" style="padding: 15px;" class="block cursor-pointer capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-plus-circle text-xs mt-1"></i>
        <span style="padding: 15px;" class="">Créer un stage</span>
      </a>

      <a href="/profile/traineeship/me" onclick="loading.start()" style="padding: 15px;" class="block cursor-pointer capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-list text-xs mt-1"></i>
        <span style="padding: 15px;" class="">mes stages</span>
      </a>
      @endif

      @if(\Auth::user()->accountType === 'person')
      <a href="/profile/mon-cv" onclick="loading.start()" style="padding: 15px;" class="block cursor-pointer capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-file text-xs mt-1"></i>
        <span style="padding: 15px;" class="">mon CV</span>
      </a>
      @endif

      <a href="/profile" onclick="loading.start()" style="padding: 15px;" class="block cursor-pointer capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-pen text-xs mt-1"></i>
        <span style="padding: 15px;" class="">Editer le profil</span>
      </a>
      
      
     <!-- Logout -->
      <a onclick="loading.start();event.preventDefault();
        document.getElementById('logout-form').submit();" style="padding: 15px;" class="block cursor-pointer capitalize mt-5 cls-side-nav-li rounded-sm text-sm">
        <i class="fas fa-sign-out-alt text-xs mt-1"></i>
        <span style="padding: 15px;" class="">Déconnexion</span>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
      </a>

      <div style="padding: 15px;" class="cls-letter-space-px uppercase text-xs leading-normal mt-5 cls-p-x-0">
        <span style="padding: 15px;" class="cls-p-0">tous les droits sont réservés à Hamza nouali et omar ourak</span>
      </div>
    </div>

    <div style="padding: 15px;" class="w-full lg:w-4/5 cls-form-container cls-p-0">

      <div style="padding: 15px;" class="border-b shadow relative bg-white flex sticky pin-t z-10">
        
        <!-- case 1: we user have not readed notifications -->
        @if(count(\Auth::user()->unreadNotifications) > 0)
          <i onclick="window.location.href = window.location.origin + '/profile/notifications'" class="cursor-pointer fas fa-bell leading-normal p-2 border border-red-light text-red-light absolute pin-r pin-t m-5 mr-12 cls-p-x-0" style="
          width: 40px;
          border-radius: 50px;
          text-align: center;
          "> <span class="absolute text-white pin-b pin-l bg-red-light rounded-full font-normal -mb-4 -ml-4 w-8 h-8 py-2 text-center text-sm">{{ count(\Auth::user()->unreadNotifications) }}</span> </i>
        @else 
          <i onclick="window.location.href = window.location.origin + '/profile/notifications'" class="cursor-pointer fas fa-bell ml-5 leading-normal p-2 absolute pin-r pin-t m-5 mr-12 cls-p-x-0" style="
              border: 1px solid #ccc;
              width: 40px;
              border-radius: 50px;
              text-align: center;
          "></i>
        @endif

        <div style="padding: 15px;" class="ml-4 w-48 flex items-start cls-p-x-0 cls-p-y-0 overflow-hidden">
          <div style="padding: 15px;" class="cls-side-nav-img-container cls-p-y-0">
            <img id="avatar" src="{{ asset('/storage/'.$user->avatar) }}"
              alt="alt placeholder" style="@if($user->accountType == 'company')padding: 15px !important;@endif" class="rounded cls-p-0 @if($user->accountType != 'company')w-12 @else bg-grey-light @endif h-12 mt-2">
          </div>
          <div style="padding: 15px;" class="w-3/5 cls-p-x-0 cls-p-y-0 ml-2 capitalize">
            <span style="padding: 15px;" class="cls-p-0 capitalize block w-full mt-3 text-sm">{{ $user->firstName }}</span>
            <span style="padding: 15px;" class="cls-p-0 capitalize block w-full mt-2 text-sm">{{ $user->lastName }}</span>
          </div>
        </div>
      </div>
      
      <div style="padding: 15px;" class="w-full mx-auto">

        @yield('content_1')
        
      </div>
    </div>

  </section>
@endsection