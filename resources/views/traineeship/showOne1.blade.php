@extends('layouts.app')

@section('content')
<div id="IndexApp" style="padding: 15px;" class="bg-white w-full relative cls-form-container cls-p-0">

  @include('layouts.navigation')

      <!--parent flex container for: cards container and filter-->
  <div style="padding: 15px;" class="cls-p-x-0 container mx-auto flex flex-wrap lg:flex-no-wrap items-start flex-row-reverse my-8">
    <!--cards container-->
    <div style="padding: 15px;" class="w-full lg:w-2/3 flex flex-wrap items-start cls-p-x-0 relative">

      <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">latest offers</h1>
      <div class="w-full">
        @include('layouts.cards')
      </div>      
      
        <div style="padding: 15px;" class="w-full">
            <div class="w-full cls-small-gradient rounded" style="padding: 15px;">
                <h1 style="padding: 15px;" class="text-white font-thin text-3xl">vous ne trouvez pas votre besoin? Le formulaire de recherche est ici.</h1>
                <div style="padding: 15px;" class="">
          
                <form onsubmit="search(event)" style="padding: 15px;" class="flex cls-search-container rounded" method="get">
                  <input id="search_input" type="text" placeholder="Cherchez..." class="w-full mr-2 p-3 rounded" style="">
                  <button class="bg-white rounded py-3 px-4 ml-1" style=""><i class="fas fa-search"></i></button>
                </form>
 
          </div>
        </div>
      </div>
    </div>
    <!--checkboxs container-->
    <div style="padding: 15px;" class="w-full lg:w-1/3 flex flex-wrap items-start cls-p-x-0 cls-blueDarker-color">
      
      <?php $filterName = 'branches';
      $filterIcon="fas fa-university";
      $filter="category";
      $list = $categories; ?>
      @include('layouts.filter')
      

    </div>
  </div>
</div>

<script>
  function search(e) {
    e.preventDefault();
    window.loading.start();
    const searchValue = document.querySelector('#search_input').value;
    window.location.href = window.location.origin + '/traineeships/search/' + searchValue;
  }
</script>

@endsection
