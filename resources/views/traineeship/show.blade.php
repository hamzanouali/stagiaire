@extends('layouts.sideNavTopNav')
@section('content_1')

  <div style="padding: 15px;" class="w-full cls-p-x-0 mx-auto flex flex-wrap items-start">
    <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">votre stages</h1>

    @include('layouts.cards')

    <!--pagination buttons-->
    <div class="w-full mt-2 cls-pagination-links-container flex text-sm" style="padding: 15px;">
      {{ $traineeships->links() }}
    </div>
  </div>

  <script>
    function openSlide(event) {
      event.stopPropagation();
      event.preventDefault();
      event.target.parentNode.className = 
          event.target.parentNode.className.replace('slideOutUp', 'slideInDown');
      event.target.parentNode
        .querySelector('#slide').style.display = 'block';
    }

    function closeSlide(event) {
      event.stopPropagation();
      event.preventDefault();
      event.target.parentNode.className = 
          event.target.parentNode.className.replace('slideInDown', 'slideOutUp');
      setTimeout(() => {
        event.target.parentNode.style.display = 'none';
        event.target.parentNode.className = 
          event.target.parentNode.className.replace('slideOutUp', 'slideInDown');
      }, 200);
    }

    function toDeletePage(event, id) {
      loading.start();
      event.stopPropagation();
      event.preventDefault();
      axios.delete('/traineeship/delete/' + id, {
          _token: document.querySelector('input[name=_token]').value
        }).then(response => {
          loading.stop();
          document.querySelector('#tr_'+id).remove();
        });
    }

    function toUpdatePage(event, id) {
      loading.start();
      event.stopPropagation();
      event.preventDefault();
      window.location.href = window.location.origin += '/traineeship/update/' + id;
    }
  </script>
@endsection