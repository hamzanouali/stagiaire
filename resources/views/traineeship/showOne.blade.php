@extends('layouts.sideNavTopNav')
@section('content_1')
  <div style="padding: 15px;" class="w-full cls-p-x-0 mx-auto flex flex-wrap items-start">
    <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">votre profile</h1>


    <div style="padding: 15px;" class="block w-full cls-p-y-0">
      <div style="padding: 15px;" class="relative overflow-hidden w-full rounded mb-5 bg-white border border-grey cursor-pointer cls-post-card">

        <i onclick="openSlide(event)" class="fas fa-ellipsis-h text-sm absolute pin-r pin-t m-5 p-1 hover:text-grey-dark" style=""></i>
        <div id="slide" style="display:none;padding: 15px;" class="animated slideInDown faster absolute pin-t pin-x bg-white shadow-lg text-sm text-center">
            <button onclick="toDeletePage(event, {{ $traineeship->id }})" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-delete-btn uppercase mx-2">supprimer</button>
            <button onclick="toUpdatePage(event, {{ $traineeship->id }})" style="padding: 15px;" class="border-2 border-black text-black cls-btn-small cls-update-btn uppercase mx-1">mettre à jour</button>
            <i onclick="closeSlide(event)" class="fas fa-times-circle absolute pin-r pin-t m-5 p-1 hover:text-grey-dark" style=""></i>
        </div>

        <div style="padding: 15px;" class="cls-p-x-0 items-start flex leading-normal cls-p-y-0">
          <img src="/storage/users/{{ $traineeship->company->user->id }}.png"
            alt="alt placeholder" style="padding: 15px;" class="cls-post-brand-logo">
          <span style="padding: 15px;" class="text-sm cls-p-y-0 ">{{ $traineeship->company->companyName }}</span>
          <i class="far fa-clock text-xs mt-1" style=""></i>
          <i style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">{{ date('Y-m-d', strtotime($traineeship->created_at)) }}</i>
        </div>
        <div style="padding: 15px;" class="cls-p-x-0">
          <h1 style="padding: 15px;" class="text-xl capitalize cls-p-x-0 leading-normal">{{ $traineeship->title }}</h1>
        </div>
        <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0">
          <i class="fas fa-map-marker-alt text-xs mt-1" style=""></i>
          <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">Alger</span>
          <i class="far fa-user-circle text-xs mt-1" style=""></i>
          <span style="padding: 15px;" class="text-sm cls-p-y-0 cls-p-l-0 ml-2">{{ $traineeship->maxPlaces }} places</span>
        </div>
        <div style="padding: 15px;" class="cls-p-x-0 cls-p-y-0 mt-5">
          <p>{{ $traineeship->description }}</p>
        </div>
      </div>
    </div>

  </div>

  <script>
    function openSlide(event) {
      event.stopPropagation();
      event.preventDefault();
      event.target.parentNode.className = 
          event.target.parentNode.className.replace('slideOutUp', 'slideInDown');
      event.target.parentNode
        .querySelector('#slide').style.display = 'block';
    }

    function closeSlide(event) {
      event.stopPropagation();
      event.preventDefault();
      event.target.parentNode.className = 
          event.target.parentNode.className.replace('slideInDown', 'slideOutUp');
      setTimeout(() => {
        event.target.parentNode.style.display = 'none';
        event.target.parentNode.className = 
          event.target.parentNode.className.replace('slideOutUp', 'slideInDown');
      }, 200);
    }

    function toDeletePage(event, id) {
      loading.start();
      event.stopPropagation();
      event.preventDefault();
      axios.delete('/traineeship/delete/' + id, {
          _token: document.querySelector('input[name=_token]').value
        }).then(response => {
          loading.stop();
          document.querySelector('#tr_'+id).remove();
        });
    }

    function toUpdatePage(event, id) {
      loading.start();
      event.stopPropagation();
      event.preventDefault();
      window.location.href = window.location.origin += '/traineeship/update/' + id;
    }
  </script>
@endsection