@extends('layouts.app')

@section('content')
<div style="padding: 15px;" class="bg-white w-full relative cls-form-container cls-p-0">

  @include('layouts.navigation')
      <!--parent flex container for: cards container and filter-->
  <div style="padding: 15px;" class="cls-p-x-0 container mx-auto flex flex-wrap lg:flex-no-wrap items-start flex-row-reverse my-8">
    <!--cards container-->
    <div style="padding: 15px;" class="w-full lg:w-2/3 flex flex-wrap items-start cls-p-x-0 relative">

      <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">latest offers</h1>
      <div class="w-full">
        @include('layouts.cards')
      </div>      

      <!--pagination buttons-->
      <div class="w-full mt-2 cls-pagination-links-container flex text-sm" style="padding: 15px;">
        {{ $traineeships->links() }}
      </div>
      
      <div class="mt-2 cls-pagination-links-container flex text-sm absolute pin-t pin-r " style="padding: 15px;">
        {{ $traineeships->links() }}
      </div>
      
        <div style="padding: 15px;" class="w-full">
            <div class="w-full cls-small-gradient rounded" style="padding: 15px;">
                <h1 style="padding: 15px;" class="text-white font-thin text-3xl">vous ne trouvez pas votre besoin? Le formulaire de recherche est ici.</h1>
                <div style="padding: 15px;" class="">
          
                <form onsubmit="search(event)" style="padding: 15px;" class="flex cls-search-container rounded" method="get">
                  <input id="search_input" type="text" placeholder="Cherchez..." class="w-full mr-2 p-3 rounded" style="">
                  <button class="bg-white rounded py-3 px-4 ml-1" style=""><i class="fas fa-search"></i></button>
                </form>
 
          </div>
        </div>
      </div>
    </div>
    <!--checkboxs container-->
    <div style="padding: 15px;" class="w-full lg:w-1/3 flex flex-wrap items-start cls-p-x-0 cls-blueDarker-color">

      <?php 
      $filterName = 'branches';
      $filterIcon="fas fa-university";
      $list = $categories; 
      $filter = 'category';
      ?>
      @include('layouts.filter')
      
      <?php 
      $filterName = 'Location';
      $filterIcon="fas fa-map-marker-alt";
      $list = [
        ['id' => 0, 'name' => 'alger'],
        ['id' => 1, 'name' => 'Blida']
      ];
      $filter = 'location'; 
      ?>
      <!-- Filter -->
      <div class="w-full relative" style="padding: 15px;">
        
        <div style="padding: 15px;" class="w-full bg-black rounded mb-2 cls-filter-container">
          <div style="padding: 15px;" class="mb-5 capitalize cls-filter-container text-lg">
            <i class="{{ $filterIcon }} -ml-3" style=""></i>
            <span class="ml-3" style="">Location</span>
          </div>
          
          <div>
            <div class="w-full border-b cls-filter-li"></div>
            <select oninput="selectLocation(event)" id="locationFilter" :ref="'checkbox_'+item.id" @click="checkIt(item.id)" :key="item.id" v-for="item in list_1"  style="padding: 15px;
    background-color: #22292f;
    width: 100%;
    color: white;" class="border-b border-l border-r cls-filter-li cursor-pointer">
              @foreach($list as $item)
                <option value="{{ $item['name'] }}">{{ $item['name'] }}</option>
              @endforeach
            </select>

            <script>
              function selectLocation(event) {
                const element = document.querySelector('#filterExecuter');
                element.setAttribute('location', event.srcElement.value);
              }
            </script>

          </div>

        </div>

      </div>

    </div>
  </div>
</div>

<script>
  function search(e) {
    e.preventDefault();
    window.loading.start();
    const searchValue = document.querySelector('#search_input').value;
    window.location.href = window.location.origin + '/traineeships/search/' + searchValue;
  }
</script>


@endsection