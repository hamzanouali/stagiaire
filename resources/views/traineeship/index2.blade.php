<div style="padding: 15px;" class="bg-white w-full relative cls-form-container cls-p-0">

  <!--navigation starts here-->
  <div style="padding: 15px;" class="border-b shadow relative bg-white flex sticky pin-t cls-z-1">
                            <div style="padding: 15px;" class="container mx-auto relative cls-p-y-0 cls-p-x-0">
      <img src="https://uploads-ssl.webflow.com/5bcb46130508ef456a7b2930/5c7121203f8bb20f5623ca81_invision-logo-export-v1.png" alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24 cls-not-bright ml-3 lg:ml-5">
      <i class="fas fa-th ml-5 leading-normal p-2 absolute pin-r pin-t mr-4 cls-p-x-0" style=""></i>
      <button style="padding: 15px;" class="float-right bg-yellow capitalize mr-16"><span>exécuter le filtre</span>
        <i class="ml-3 animated slideInLeft infinite fas fa-long-arrow-alt-right" style=""></i></button>
      <button style="padding: 15px;" class="float-right bg-grey-lightest capitalize mr-3"><span>annuler</span>
        <i class="ml-3 fas fa-times-circle" style=""></i></button></div>
  </div>
      <!--parent flex container for: cards container and filter-->
  <div style="padding: 15px;" class="cls-p-x-0 container mx-auto flex flex-wrap lg:flex-no-wrap items-start flex-row-reverse my-8">
    <!--cards container-->
    <div style="padding: 15px;" class="w-full lg:w-2/3 flex flex-wrap items-start cls-p-x-0 relative">

      <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">latest offers</h1>
      <div style="padding: 15px;" class="w-full cls-p-y-0">
        <?php $cardWidth = 'w-full'; ?>
        @include('layouts.cards')
      </div>      

      <!--pagination buttons-->
      <div class="w-full mt-2 cls-pagination-links-container flex text-sm" style="padding: 15px;">
        <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1"><i class="fas fa-long-arrow-alt-left"></i></a>
        <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">1</a>
                <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">2</a>
        <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
          <i class="fas fa-long-arrow-alt-right" style=""></i></a></div>
            <div class="mt-2 cls-pagination-links-container flex text-sm absolute pin-t pin-r " style="padding: 15px;">
        <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1"><i class="fas fa-long-arrow-alt-left"></i></a>
        <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">1</a>
                <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">2</a>
        <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
          <i class="fas fa-long-arrow-alt-right" style=""></i></a></div>
            <div style="padding: 15px;" class="w-full">
                <div class="w-full cls-small-gradient rounded" style="padding: 15px;">
                    <h1 style="padding: 15px;" class="text-white font-thin text-3xl">vous ne trouvez pas votre besoin? Le formulaire de recherche est ici.</h1>
                    <div style="padding: 15px;" class="">
                        <div style="padding: 15px;" class="flex cls-search-container rounded">
              <input disabled="true" type="text" placeholder="Cherchez..." class="w-full mr-2 p-3 rounded" style="">
              <button class="bg-white rounded py-3 px-4 ml-1" style=""><i class="fas fa-search"></i></button></div>
          </div>
        </div>
      </div>
    </div>
    <!--checkboxs container-->
    <div style="padding: 15px;" class="w-full lg:w-1/3 flex flex-wrap items-start cls-p-x-0 cls-blueDarker-color">
      
      <!-- Filter -->
      <div class="w-full" style="padding: 15px;">
        
        <div style="padding: 15px;" class="w-full bg-black rounded mb-6 cls-filter-container">
          <div style="padding: 15px;" class="mb-5 capitalize cls-filter-container text-lg">
            <i class="fas fa-university -ml-3" style=""></i>
            <span class="ml-3" style="">les branches</span>
          </div>
          <div style="padding: 15px;" class="border-b border-l border-r border-t cls-filter-li  cls-filter-checked">
            <i class="fas fa-check-square  mt-1" style=""></i>
            <span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-5 capitalize">touts</span>
          </div>
        
          @foreach($categories as $category) 
            <div style="padding: 15px;" class="border-b border-l border-r cls-filter-li">
              <i class="fas fa-square  mt-1" style=""></i>
              <span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-5 capitalize">$category->name</span>
            </div>
          @endforeach
        </div>

      </div>

    </div>
  </div>
</div>
















@extends('layouts.app')

@section('content')
<div style="padding: 15px;" class="bg-white w-full relative cls-form-container cls-p-0">

  <!--navigation starts here-->
  <div style="padding: 15px;" class="bg-white z-10 border-b shadow relative bg-white flex sticky pin-t">
                            <div style="padding: 15px;" class="container mx-auto relative cls-p-y-0 cls-p-x-0">
      <img src="{{ asset('storage/logo.png') }}" alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24 cls-not-bright ml-3 lg:ml-5">
      <i class="fas fa-th ml-5 leading-normal p-2 absolute pin-r pin-t mr-4 cls-p-x-0" style=""></i></div>
  </div>
      <!--parent flex container for: cards container and filter-->
  <div style="padding: 15px;" class="cls-p-x-0 container mx-auto flex flex-wrap lg:flex-no-wrap items-start flex-row-reverse my-16">
    <!--cards container-->
    <div style="padding: 15px;" class="w-full lg:w-2/3 flex flex-wrap items-start cls-p-x-0">
      <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">latest offers</h1>
      <div style="padding: 15px;" class="w-full cls-p-y-0">
        <?php $cardWidth = 'w-full'; ?>
        @include('layouts.cards')
      </div>
      <!--pagination buttons-->
      <div style="padding: 15px;" class=" w-full">
        {{ $traineeships->links() }}
        <!-- <i class="cls-pagination-btn fas fa-angle-double-left" style=""></i>
        <span style="padding: 15px;" class="cls-pagination-btn">1</span>
        <span style="padding: 15px;" class="cls-pagination-btn">2</span>
        <span style="padding: 15px;" class="cls-pagination-btn">3</span>
        <i class="cls-pagination-btn fas fa-angle-double-right" style=""></i> -->
      </div>
    </div>
    <!--checkboxs container-->
    <div style="padding: 15px;" class="w-full lg:w-1/3 flex flex-wrap items-start cls-p-x-0 cls-blueDarker-color">
      <!--this must be the first-->
      <h1 class="w-full block font-normal text-grey-darker text-xl mt-5 mb-8 w-full mx-5 capitalize" style="">les branches</h1>
                        <!--checkbox container-->
      <div style="padding: 15px;" class="w-full cls-p-y-0">

        <div style="padding: 15px;" class="cls-checked ml-2 border-l border-r border-t border-grey rounded-t">
          <i class="fas fa-check-square  mt-1" style=""></i><span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-2 capitalize">touts</span>
        </div>
        @foreach($categories as $category) 
          <div style="padding: 15px;" class="ml-2 border-l border-r border-t border-grey @if($category->id == $categories[count($categories)-1]->id) rounded-b border-b @endif">
            <i class="fas fa-square  mt-1" style=""></i><span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ $category->name }}</span>
          </div>
        @endforeach
      </div>
      
      <h1 class="w-full block font-normal text-grey-darker text-xl mt-8 mb-8 w-full mx-5 capitalize" style="">region</h1>
      <div style="padding: 15px;" class="w-full cls-p-y-0">

        <div style="padding: 15px;" class="cls-checked ml-2 border-l border-r border-t border-grey rounded-t">
          <i class="fas fa-check-square  mt-1" style=""></i><span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-2 capitalize">touts</span>
        </div>
        @foreach($locations = ['nord', 'sud', 'est', 'west'] as $location) 
          <div style="padding: 15px;" class="ml-2 border-l border-r border-t border-grey @if($location == $locations[count($locations)-1]) rounded-b border-b @endif">
            <i class="fas fa-square  mt-1" style=""></i><span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ $location }}</span>
          </div>
        @endforeach

      </div>

      <h1 class="w-full block font-normal text-grey-darker text-xl mt-8 mb-8 w-full mx-5 capitalize" style="">location</h1>
      <div style="padding: 15px;" class="w-full cls-p-y-0">

        <div style="padding: 15px;" class="cls-checked ml-2 border-l border-r border-t border-grey rounded-t">
          <i class="fas fa-check-square  mt-1" style=""></i><span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-2 capitalize">touts</span>
        </div>
        @foreach($locations = ['alger', 'oran', 'tipaza', 'blida'] as $location) 
          <div style="padding: 15px;" class="ml-2 border-l border-r border-t border-grey @if($location == $locations[count($locations)-1]) rounded-b border-b @endif">
            <i class="fas fa-square  mt-1" style=""></i><span style="padding: 15px;" class="cls-p-y-0 cls-p-l-0 ml-2 capitalize">{{ $location }}</span>
          </div>
        @endforeach

      </div>
    </div>
  </div>
</div>
@endsection