@if ($paginator->hasPages())
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <!-- <a style="padding: 15px;" class="block text-center cursor-not-allowed bg-grey w-12 border-grey border rounded-sm mx-1">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a> -->
    @else
        <a href="{{ $paginator->previousPageUrl() }}" style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
            <i class="fas fa-long-arrow-alt-left"></i>
        </a>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
        {{-- "Three Dots" Separator --}}
        @if (is_string($element))
            <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
                {{ $element }}
            </a>
        @endif

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <a style="padding: 15px;" class="block bg-grey text-center w-12 cursor-not-allowed border-grey border rounded-sm mx-1">
                        {{ $page }}
                    </a>
                @else
                    <a href="{{ $url }}" style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
                        {{ $page }}
                    </a>
                @endif
            @endforeach
        @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
            <i class="fas fa-long-arrow-alt-right"></i>
        </a>
    @else
        <!-- <a style="padding: 15px;" class="block cursor-not-allowed bg-grey text-center text-grey w-12 border-grey border rounded-sm mx-1">
            <i class="fas fa-long-arrow-alt-right"></i>
        </a> -->
    @endif
@endif


<!-- <div class="w-full mt-2 cls-pagination-links-container flex text-sm" style="padding: 15px;">
    <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
        <i class="fas fa-long-arrow-alt-left"></i>
    </a>
    <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">1</a>
    <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">2</a>
    <a style="padding: 15px;" class="block text-center w-12 bg-white border-grey border rounded-sm mx-1">
        <i class="fas fa-long-arrow-alt-right" style=""></i>
    </a>
</div> -->