
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ asset('assets/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/tailwindcss/dist/tailwind.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/style.css') }}" rel="stylesheet">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <style>
          a {
            text-decoration: none;
            color: inherit;
          }
        </style>

      </head> 
      <body class="cls-main-body">
        <div class="cls-header" style="padding: 15px;">




<div style="padding: 15px;" class="cls-p-y-0 w-full"><div style="padding: 15px;" class="cls-navigation container mx-auto flex">
  <img src="{{ asset('storage/logo.png') }}" alt="alt placeholder" style="padding: 15px;" class="cls-logo h-24">
  
  <a href="" style="padding: 15px;" class="cls-navigation-link">Les stages</a>
  <a href="" style="padding: 15px;" class="cls-navigation-link">Contactez nous</a>
  <a href="{{ route('register') }}" style="padding: 15px;" class="cls-navigation-link cls-special-btn">Commencez</a></div></div><div style="min-height: 100px;" class="cls-header-container container mx-auto">
  
  <div style="min-height: 60px;" class="cls-header-text">
    
    
    <p style="" class="cls-header-title">Nous proposons des stages proposés par des entreprises à des étudiants <b>algériens.</b>  </p>
    <button style="" class="cls-button">voir plus</button>
  </div>
</div></div><div style="padding: 15px;" class="cls-body">

<div style="padding: 15px;" class="cls-section-container container mx-auto mt-12 text-center">
  
  <span style="padding: 15px;" class="cls-first-label cls-hover-before">Quel est le problème ?</span>
  
  <div style="padding: 15px;" class="flex flex-wrap lg:flex-no-wrap mt-12">
    
    <div style="padding: 15px;" class="w-full">
      
      <p style="padding: 15px;" class="cls-desc-text">plus
        <b class="cls-highlighted cls-hover-before" style="">de 2 millions</b> d'étudiants en <b class="cls-highlighted cls-hover-before" style="">algérie</b> cherchent une occasion de mettre en pratique ce qu'ils ont appris dans leurs universités et nous voulons les aider.</p>
    </div>
    
    <div style="padding: 15px;" class="w-full">
      <img src="storage/vector1.png" alt="alt placeholder" style="padding: 15px;" class="h-64"></div>
  </div>
</div>

<div style="padding: 15px;" class="cls-section-container container mx-auto mt-12 text-center">
  
  <span style="padding: 15px;" class="cls-first-label cls-hover-before">Notre solution</span>
  
  <div style="padding: 15px;" class="flex flex-wrap lg:flex-no-wrap mt-12 flex-row-reverse">
    
    <div style="padding: 15px;" class="w-full">
      
      <p style="padding: 15px;" class="cls-desc-text">Nous fournissons <b class="cls-highlighted cls-hover-before" style="">cette plate-forme</b> afin d'établir une
        <b class="cls-highlighted cls-hover-before" style="">relation</b> entre nos <b class="cls-highlighted cls-hover-before" style="">étudients</b> et
        <b class="cls-highlighted cls-hover-before" style="">les entreprises</b>. </p>
    </div>
    
    <div style="padding: 15px;" class="w-full">
      <img src="storage/vector2.svg" alt="alt placeholder" style="padding: 15px;" class="h-64"></div>
  </div>
</div>
<div style="padding: 15px;" class="cls-section-container container mx-auto mt-12 text-center">
  
  <span style="padding: 15px;" class="cls-first-label cls-hover-before">confiance par</span>
  
  <div style="padding: 15px;" class="flex flex-wrap lg:flex-no-wrap mt-12 flex-row-reverse">
    
    
    <div style="padding: 15px;" class="w-full">
      <img src="https://uploads-ssl.webflow.com/5bcb46130508ef456a7b2930/5c71212e6e5e68afc3eab981_toptal-logo-export-v1.png" alt="alt placeholder" style="padding: 15px;" class="h-24"></div>
    <div style="padding: 15px;" class="w-full">
      <img src="https://uploads-ssl.webflow.com/5bcb46130508ef456a7b2930/5c7121203f8bb20f5623ca81_invision-logo-export-v1.png" alt="alt placeholder" style="padding: 15px;" class="h-24"></div>
    <div style="padding: 15px;" class="w-full">
      <img src="https://uploads-ssl.webflow.com/5bcb46130508ef456a7b2930/5c71210dc6a0652e7a79e8ac_amazon-logo-export-v1.png" alt="alt placeholder" style="padding: 15px;" class="h-24"></div>
    <div style="padding: 15px;" class="w-full">
      <img src="https://uploads-ssl.webflow.com/5bcb46130508ef456a7b2930/5c7120fa2d05cec571969c2d_google-logo-export-v1.png" alt="alt placeholder" style="padding: 15px;" class="h-24"></div>
  </div>
</div>

</div><div style="padding: 15px;" class="cls-footer"><div style="padding: 15px;" class="cls-footer-container text-center container mx-auto"><i style="padding: 15px;" class="far fa-lightbulb text-5xl mb-8"></i><h1 style="padding: 15px;" class="w-full font-thin lg:w-4/5 mx-auto leading-normal">Vous pouvez nous faire confiance en quelques étapes et clics et vous êtes avec nous, en tant qu’entreprise ou étudiant.</h1><button style="" class="cls-button">joindre</button></div><div style="padding: 15px;" class="cls-footer-container container mt-10 mx-auto flex flex-wrap lg:flex-no-wrap"><div style="padding: 15px;" class="lg:w-1/2 w-full"><h1 style="padding: 15px;" class="text-2xl font-bold cls-p-x-0">Contact us</h1><ul style="padding: 15px;" class="cls-footer-ul list-reset cls-p-x-0">
    <li style="" class=""><i class="fab fa-facebook mr-2"></i>
<span style="" class="">facebook</span></li>
  <li style="" class=""><i class="fab fa-twitter mr-2" style=""></i>
<span style="" class="">twitter</span></li><li style="" class=""><i class="fas fa-envelope mr-2" style=""></i>
<span style="" class="">email@somthing.com</span></li><li style="" class=""><i class="fas fa-phone mr-2" style=""></i>
<span style="" class="">+2136546465498</span></li></ul></div><div style="padding: 15px;" class="cls-shape lg:w-1/2 w-full"><p style="padding: 15px;" class="cls-quoted-p leading-normal text-xl font-thin">L'absence d'un moyen permettant de rapprocher les étudiants des universités des fondations ou des entreprises afin de leur permettre de former des stagiaires est considérée comme un problème énorme qui doit être résolu, et c'est ce que nous faisons.</p><b style="padding: 15px;" class="">Co fondateurs ( Hamza Nouali &amp; Omar Ourak )</b></div></div><div style="padding: 15px;" class="container mx-auto mt-5 text-center"><p style="padding: 15px;" class="cls-letter-space-px uppercase text-xs leading-normal">tous les droits sont réservés à Hamza nouali et omar ourak</p></div></div>
      </body>
      </html>